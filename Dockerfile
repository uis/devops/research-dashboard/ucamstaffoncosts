# The docker image built from this Dockerfile is intended only to run the test
# suite and to build the documentation. Installation of this project should be
# performed via pip.
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:3.11-alpine
ADD ./ /usr/src/app/
WORKDIR /usr/src/app
RUN pip install tox && pip install -e .
