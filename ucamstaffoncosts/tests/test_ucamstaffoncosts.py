import csv
import hashlib
import io
import os
import unittest.mock as mock
from datetime import datetime
from freezegun import freeze_time
from unittest import TestCase

import ucamstaffoncosts.costs as costs
from ucamstaffoncosts.util import calculate_commitment_from_date, _calculate_pay_day


TEST_DATA_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)),  'test_data')
PUBLIC_DATA_DIR = os.path.join(TEST_DATA_DIR, 'public')
PRIVATE_DATA_DIR = os.path.join(TEST_DATA_DIR, 'private')


# Load the SHA256 hashes of the correct tables from the private data directory. A dictionary keyed
# by original file name giving the hash of the correct file.
PRIVATE_DATA_HASHES = dict(
    line.split()[::-1] for line in open(os.path.join(PRIVATE_DATA_DIR, 'on-costs-hashes.txt'))
)


class TestUcamOnCosts(TestCase):

    def test_not_implemented_scheme(self):
        """Check NotImplementedError is raised if the scheme is unknown."""

        with self.assertRaises(NotImplementedError):
            # Bad year
            costs.calculate_cost(100, -1000, costs.Scheme.USS)

        with self.assertRaises(NotImplementedError):
            # Bad year
            costs.calculate_cost(100, 2018, 'this-is-not-a-pension-scheme')

    def test_no_scheme_2018(self):
        """Check on-costs if employee has no pension scheme."""
        self.assert_generator_matches_table(
            lambda s: costs.calculate_cost(s, costs.Scheme.NONE, 2018),
            'no_scheme_2018.csv', with_exchange_column=True)

    def test_uss_2018(self):
        """Check on-costs if employee has a USS scheme."""
        self.assert_generator_matches_table(
            lambda s: costs.calculate_cost(s, costs.Scheme.USS, 2018),
            'uss_2018.csv')

    def test_uss_2018_exchange(self):
        """Check on-costs if employee has a USS scheme with salary exchange."""
        self.assert_generator_matches_table(
            lambda s: costs.calculate_cost(s, costs.Scheme.USS_EXCHANGE, 2018),
            'uss_exchange_2018.csv', with_exchange_column=True)

    def test_cps_hybrid_2018(self):
        """Check on-costs if employee has a CPS hybrid scheme."""
        self.assert_generator_matches_table(
            lambda s: costs.calculate_cost(s, costs.Scheme.CPS_HYBRID, 2018),
            'cps_hybrid_2018.csv', with_exchange_column=True)

    def test_cps_hybrid_2018_exchange(self):
        """Check on-costs if employee has a CPS hybrid scheme with salary exchange."""
        self.assert_generator_matches_table(
            lambda s: costs.calculate_cost(s, costs.Scheme.CPS_HYBRID_EXCHANGE, 2018),
            'cps_hybrid_exchange_2018.csv', with_exchange_column=True)

    def test_cps_pre_2013(self):
        """Check on-costs if employee has a CPS pre-2013 scheme."""
        self.assert_generator_matches_table(
            lambda s: costs.calculate_cost(s, costs.Scheme.CPS_PRE_2013, 2018),
            'cps_pre_2013.csv', with_exchange_column=True)

    def test_cps_pre_2013_exchange(self):
        """Check on-costs if employee has a CPS pre-2013 scheme with salary exchange."""
        self.assert_generator_matches_table(
            lambda s: costs.calculate_cost(s, costs.Scheme.CPS_PRE_2013_EXCHANGE, 2018),
            'cps_exchange_pre_2013.csv', with_exchange_column=True)

    def test_nhs_2018(self):
        """Check on-costs if employee has a NHS scheme."""
        self.assert_generator_matches_table(
            lambda s: costs.calculate_cost(s, costs.Scheme.NHS, 2018),
            'nhs_2018.csv', with_exchange_column=True)

    def test_mrc_2018(self):
        """Check on-costs are returned for MRC."""
        # We have no ground truth for MRC unfortunately so we check that NotImplementedError is not
        # raised and a result is returned.
        result = costs.calculate_cost(40000, costs.Scheme.MRC, 2018)
        self.assertEqual(result.salary, 40000)
        self.assertGreater(result.total, result.salary)

    def test_default_scheme(self):
        """Check on-costs for default scheme and year"""
        with mock.patch('ucamstaffoncosts.costs._LATEST_TAX_YEAR', 2018):
            self.assert_generator_matches_table(
                lambda s: costs.calculate_cost(s, costs.Scheme.NHS),
                'nhs_2018.csv', with_exchange_column=True)

    def test_calculate_pay_day(self):
        """Check that calculate_pay_day() correctly calculates pay day"""

        # check that the current month's pay day is the 26th
        with freeze_time("2020-02-13"):
            self.assertEqual(_calculate_pay_day(), datetime(2020, 2, 26).date())

        # check that when the current month's 26th is a saturday - pay day is the 25th
        with freeze_time("2020-09-13"):
            self.assertEqual(_calculate_pay_day(), datetime(2020, 9, 25).date())

        # check that when the current month's 26th is a sunday - pay day is the 24th
        with freeze_time("2020-01-13"):
            self.assertEqual(_calculate_pay_day(), datetime(2020, 1, 24).date())

        # check that when the current month's 26th is a sunday and the 24th is a bank holiday -
        # pay day is the 23rd
        with freeze_time("2020-01-13"):
            bank_holidays = {datetime(2020, 1, 24).date()}
            self.assertEqual(_calculate_pay_day(bank_holidays), datetime(2020, 1, 23).date())

    def test_calculate_commitment_from_date(self):
        """Check that calculate_pay_day() correctly calculates from_date"""

        # check that the from_date is the 1st of this month, if we are before pay day
        with freeze_time("2020-02-13"):
            self.assertEqual(calculate_commitment_from_date(), datetime(2020, 2, 1).date())

        # check that the from_date is the 1st of next month, if we are after pay day
        with freeze_time("2020-02-26"):
            self.assertEqual(calculate_commitment_from_date(), datetime(2020, 3, 1).date())

    def assert_generator_matches_table(self, on_cost_generator, table_filename,
                                       with_exchange_column=False):
        """
        Take a generator callable which returns an OnCost from a base salary and check that its
        output matches a HR table. *table_filename* should be the HR table filename and
        *with_exchange_column*indicates if that table has an Exchange column.

        """
        headings, spine_rows = self.read_spine_points()

        if with_exchange_column:
            headings.append('Exchange (£)')

        headings.extend([
            'Pension (£)', 'NI (£)', 'Apprenticeship Levy (£)', 'Total (£)'
        ])

        out = io.StringIO()
        writer = csv.writer(out)
        writer.writerow(headings)

        for point, base_salary in spine_rows:
            on_cost = on_cost_generator(int(base_salary))

            row = [point, base_salary]

            if with_exchange_column:
                row.append(self.blank_if_zero(on_cost.exchange))

            row.extend([
                self.blank_if_zero(on_cost.employer_pension),
                self.blank_if_zero(on_cost.employer_nic),
                self.blank_if_zero(on_cost.apprenticeship_levy),
                self.blank_if_zero(on_cost.total),
            ])

            writer.writerow(row)

        self.assert_tables_match(out.getvalue().encode('utf8'), table_filename)

    def assert_tables_match(self, table_contents, filename):
        """
        Assert that a generate table matches the one specified by filename. *table* must be a
        bytes object.

        """
        expected_hash, expected_table_contents = self.read_table(filename)

        # If we have the expected table, compare it directly.
        if expected_table_contents is not None:
            # Compare line-wise
            for line, expected_line in zip(table_contents.decode('utf8').splitlines(),
                                           expected_table_contents.decode('utf8').splitlines()):
                self.assertEqual(line, expected_line)

            # Compare byte-wise
            self.assertEqual(table_contents, expected_table_contents)

        # Compare the hashes
        table_hash = hashlib.sha256(table_contents).hexdigest()
        self.assertEqual(table_hash, expected_hash)

    def read_spine_points(self):
        """
        Return table header and a sequence of rows from the spine points table.

        """
        with open(os.path.join(PUBLIC_DATA_DIR, 'spine_points_august_2017.csv')) as f:
            reader = csv.reader(f)
            headings = next(reader)
            return headings, list(reader)

    def read_table(self, filename):
        """
        Return the hash of the filename and its contents if present in the TEST_DATA_DIR
        directory. If the file is not present, the contents are returned as None.

        The file itself may not be present as the contents are private.

        """
        hash_value = PRIVATE_DATA_HASHES[filename]
        abs_path = os.path.join(PRIVATE_DATA_DIR, filename)

        # Return hash file contents if the file can be read, otherwise return hash and None.
        try:
            with open(abs_path, 'rb') as f:
                return hash_value, f.read()
        except IOError:
            return hash_value, None

    def blank_if_zero(self, value):
        """
        Helper function to return the string representation of a value or the blank string if the
        value is zero.

        """
        return str(value) if value != 0 else ''
