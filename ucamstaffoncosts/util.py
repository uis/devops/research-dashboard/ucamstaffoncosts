"""
General utility functions.

"""
import requests
from calendar import SATURDAY, SUNDAY
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta

BANK_HOLIDAYS = 'https://www.gov.uk/bank-holidays.json'

PAY_DAY = 26


def pprinttable(rows):
    """Adapted from: https://stackoverflow.com/questions/5909873

    >>> import collections
    >>> Row = collections.namedtuple('Row', 'foo bar buzz')
    >>> pprinttable([
    ...     Row('one', 'two', 'three'), Row(1, 2, 3),
    ... ]) # doctest: +NORMALIZE_WHITESPACE
    foo | bar | buzz
    ----+-----+------
    one | two | three
    1   | 2   | 3
    """
    headers = rows[0]._fields
    lens = []
    for i in range(len(rows[0])):
        lens.append(
            len(max([str(x[i]) for x in rows] + [headers[i]], key=lambda x: len(str(x)))))
    formats = []
    hformats = []
    for i in range(len(rows[0])):
        formats.append("%%-%ds" % lens[i])
        hformats.append("%%-%ds" % lens[i])
    pattern = " | ".join(formats)
    hpattern = " | ".join(hformats)
    separator = "-+-".join(['-' * n for n in lens])
    print(hpattern % tuple(headers))
    print(separator)

    for line in rows:
        print(pattern % tuple(str(t) for t in line))


def calculate_commitment_from_date(bank_holidays={}):
    """
    Calculate the from date used to seperate expenditure from commitment as 1st of this month if
    today is before pay else the 1st of next month.
    """
    today = date.today()
    delta = relativedelta(day=1)
    if today >= _calculate_pay_day(bank_holidays):
        delta += relativedelta(months=1)
    return today + delta


def _calculate_pay_day(bank_holidays={}, pay_day=PAY_DAY):
    """
    Calculate the university pay day as a fixed day in the month
    excluding weekends and bank holidays
    """
    pay_day = date.today().replace(day=pay_day)
    while (pay_day in bank_holidays or pay_day.weekday() in (SATURDAY, SUNDAY)):
        pay_day = pay_day - timedelta(days=1)
    return pay_day


def retrieve_bank_holidays(bank_holidays=BANK_HOLIDAYS):
    """
    Retrieve bank holiday data and return a set of english bank holidays.
    NOTE: This assumes that we are only interested in english bank holidays.
    """
    response = requests.get(BANK_HOLIDAYS)
    response.raise_for_status()
    return {
        datetime.strptime(event['date'], '%Y-%m-%d').date()
        for event in response.json()['england-and-wales']['events']
    }
