from setuptools import setup, find_packages

setup(
    name='salaries',
    version='1.2.10',
    packages=find_packages(),
    package_data={'ucamstaffoncosts': ['data/*.yaml']},
    install_requires=[
        'pyyaml',
        'python-dateutil',
        'requests',
    ]
)
